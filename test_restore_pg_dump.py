# These tests can be run with:
#     python3 -m unittest test_*.py
import unittest
from backup import get_restore_command_pg_dump


class PgDumpRestoreTest(unittest.TestCase):
    def test_pg_dump_backup_command_destdb(self):
        self.assertEqual(get_restore_command_pg_dump(dest_db="new_db"), "pg_dump -d  -F c | pg_restore")

    def test_pg_dump_backup_command_sourcedb_destdb(self):
        self.assertEqual(get_restore_command_pg_dump(source_db="original", dest_db="new_db"), "pg_dump -d original -F c | pg_restore")

    def test_pg_dump_backup_command_destdb_destfile(self):
        self.assertEqual(get_restore_command_pg_dump(dest_db="new_db", dest_file="/tmp/backup.pgdump"), "pg_dump -d  -F c -f /tmp/backup.pgdump | pg_restore")

    def test_pg_dump_backup_command_destdb_user(self):
        self.assertEqual(get_restore_command_pg_dump(dest_db="new_db", user="odoo"), "pg_dump -d  -F c -U odoo | pg_restore")

    def test_pg_dump_backup_command_destdb_verbose(self):
        self.assertEqual(get_restore_command_pg_dump(dest_db="new_db", verbose=True), "pg_dump -d  -F c -v | pg_restore")

    def test_pg_dump_backup_command_sourcedb_destdb_destfile(self):
        self.assertEqual(
            get_restore_command_pg_dump(source_db="original", dest_db="new_db", dest_file="/tmp/backup.pgdump"),
            "pg_dump -d original -F c -f /tmp/backup.pgdump | pg_restore",
        )

    def test_pg_dump_backup_command_sourcedb_destdb_user(self):
        self.assertEqual(get_restore_command_pg_dump(source_db="original", dest_db="new_db", user="odoo"), "pg_dump -d original -F c -U odoo | pg_restore")

    def test_pg_dump_backup_command_sourcedb_destdb_verbose(self):
        self.assertEqual(get_restore_command_pg_dump(source_db="original", dest_db="new_db", verbose=True), "pg_dump -d original -F c -v | pg_restore")

    def test_pg_dump_backup_command_destdb_destfile_user(self):
        self.assertEqual(get_restore_command_pg_dump(dest_db="new_db", dest_file="/tmp/backup.pgdump", user="odoo"), "pg_dump -d  -F c -U odoo | pg_restore")

    def test_pg_dump_backup_command_destdb_destfile_verbose(self):
        self.assertEqual(get_restore_command_pg_dump(dest_db="new_db", dest_file="/tmp/backup.pgdump", verbose=True), "pg_dump -d  -F c -v | pg_restore")

    def test_pg_dump_backup_command_destdb_user_verbose(self):
        self.assertEqual(get_restore_command_pg_dump(dest_db="new_db", user="odoo", verbose=True), "pg_dump -d  -F c -U odoo -v| pg_restore")

    def test_pg_dump_backup_command_sourcedb_destdb_destfile_user(self):
        self.assertEqual(
            get_restore_command_pg_dump(source_db="original", dest_db="new_db", dest_file="/tmp/backup.pgdump", user="odoo"),
            "pg_dump -d original -F c -U odoo | pg_restore",
        )

    def test_pg_dump_backup_command_sourcedb_destdb_destfile_verbose(self):
        self.assertEqual(
            get_restore_command_pg_dump(source_db="original", dest_db="new_db", dest_file="/tmp/backup.pgdump", verbose=True),
            "pg_dump -d original -F c -v | pg_restore",
        )

    def test_pg_dump_backup_command_sourcedb_destdb_user_verbose(self):
        self.assertEqual(
            get_restore_command_pg_dump(source_db="original", dest_db="new_db", user="odoo", verbose=True), "pg_dump -d original -F c -U odoo -v | pg_restore"
        )

    def test_pg_dump_backup_command_destdb_destfile_user_verbose(self):
        self.assertEqual(get_restore_command_pg_dump(dest_db="new_db", dest_file="/tmp/backup.pgdump", user="odoo", verbose=True), "pg_dump -d  -F c -U odoo -f /tmp/backup.pgdump -v | pg_restore")

    def test_pg_dump_backup_command_sourcedb_destdb_destfile_user_verbose(self):
        self.assertEqual(
            get_restore_command_pg_dump(source_db="original", dest_db="new_db", dest_file="/tmp/backup.pgdump", user="odoo", verbose=True), "pg_dump -d original -F c -U odoo -f /tmp/backup.pgdump -v | pg_restore"
        )


if __name__ == "__main__":
    unittest.main()
