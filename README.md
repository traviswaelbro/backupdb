# backupdb

```
usage: backup.py [-h] (--psql | --pgdump) --sourcedb SOURCEDB
                 [--destdb DESTDB] [--file FILE] [--sudo] [--user USER]
                 [--verbose]

Back Up PostgreSQL Database

optional arguments:
  -h, --help            show this help message and exit
  --psql                Backup database using a standard psql command
  --pgdump              Backup database using a compressed pg_dump
  --sourcedb SOURCEDB, -s SOURCEDB
                        Name of Source Database (to be backed up)
  --destdb DESTDB, -d DESTDB
                        Name of Destination Database (to be created)
  --file FILE, -f FILE  File name (with full or relative path) where backup
                        should be stored
  --sudo, -S            Run backup/restore commands with sudo
  --user USER, -U USER  PostgreSQL user to use for backup and/or restore
  --verbose, -v

    Examples:
        # Copy existing database to a new database
        python3 backup.py --pgdump --sourcedb BHS_Production --destdb BHS_Testing

        # Copy existing database and save the backup to a file
        python3 backup.py --pgdump --sourcedb BHS_Production --file=/tmp/backups/BHS_Production_20190501.pgdump

        # Copy existing database to a new database and save the backup to a file
        python3 backup.py --pgdump --sourcedb BHS_Production --destdb BHS_Testing --file=/tmp/backups/BHS_Production_20190501.pgdump
```

## testing

```
python3 -m unittest test_*.py
```
