import argparse
from pprint import pprint as pp


def get_backup_command_pg_dump(source_db='', dest_file='', user='', verbose=False):
    """
    Build out the `pg_dump` command to be used for this backup.

    Example output:
        pg_dump -d BHS_Production -F c -U postgres -f /tmp/BHS_Production_2019-04-02.pgdump -v

    :param source_db {str}: Name of the existing database to back up.
    :param dest_file {str}:
        (Optional) Name (with path) of the file where the backup will be stored.
    :param user {str}: PostgreSQL user name to use for backup.
    :param verbose {bool}: If True, run backup command with verbose flag.
    :return {str}: The pg_dump command to be executed
    """
    backup_command = f"""pg_dump -d {source_db} -F c"""
    if user:
        backup_command += f" -U {user}"
    if dest_file:
        backup_command += f" -f {dest_file}"
    if verbose:
        backup_command += " -v"
    # TODO: Include --exclude-table-data to ignore contents of specific
    #       tables. This could be useful for skipping attachments, messages,
    #       email logger, etc. It can be provided to pg_dump multiple times,
    #       so this script could accept a comma-separated list of
    #       tables/patterns.

    return backup_command


def get_restore_command_pg_dump(dest_db='', source_db='', source_file='', user='', verbose=False):
    """
    Build out the `pg_restore` command to be used for this backup.

    Example:
        pg_restore -F c -d BHS_Testing_20190402 /tmp/BHS_Production_2019-04-02.pgdump -U postgres -v

    :param dest_db {str}: Name of the new database to be created from the backup.
    :param source_db {str}:
        (Optional) Name of the existing database to be restore from.
    :param source_file {str}:
        (Optional) Name (with path) of the file being restored from.
    :param user {str}: PostgreSQL user name to use for restore.
    :param verbose {bool}: If True, run restore command with verbose flag.
    :return {str}: The pg_restore command to be executed
    """
    #  TODO: What do we do if we're not backing up to a file?
    #        https://stackoverflow.com/questions/11103279/use-pg-dump-result-as-input-for-pg-restore
    restore_command = "pg_restore -F c -d {dest_db}"
    if source_file:
        restore_command += f" {source_file}"
    elif source_db:
        raise NotImplementedError("Restore from database is not yet supported")

    if user:
        restore_command += f" -U {user}"
    if verbose:
        restore_command += " -v"

def manage_backup(backup_type, source_db, dest_db, dest_file, sudo=False, user=None, verbose=False, **kwargs):
    """
    Perform a backup (and optionally restore) based on the provided parameters.

    :param backup_type {str}:
        Type of backup command to be used. Expects 'psql' or 'pg_dump'.
    :param source_db {str}: Name of the existing database to back up.
    :param dest_db {str}:
        (Optional) Name of the new database to be created from the backup.
    :param dest_file {str}:
        (Optional) Name (with path) of the file where the backup will be stored.
    :param sudo {bool}: If True, backup and restore commands will run with sudo.
    :param user {str}: PostgreSQL user name to use for backup and/or restore.
    :param verbose {bool}: If True, run backup command with verbose flag.
    :return {NoneType}:
    """

    if verbose:
        pp(
            {
                "Backup Type": backup_type,
                "Source Database": source_db,
                "Destination Database": dest_db,
                "Destination File": dest_file,
                "Use Sudo": sudo,
                "User": user,
                "Verbose": verbose,
                "Other Arguments": kwargs,
            }
        )

    if backup_type != "pg_dump":
        raise NotImplementedError(f"Backup type of '{backup_type}' is not yet supported")

    if backup_type == "pg_dump":
        backup_command = get_backup_command_pg_dump(source_db, dest_file, user, verbose)
        print(f"Backup Command: {backup_command}")

        if source_db:
            restore_command = get_restore_command_pg_dump(source_db, dest_db, dest_file, user)
            print(f"Restore Command: {restore_command}")


def parse_arguments():
    """
    Parse the arguments provided to this script.

    :return {dict}:
        Dictionary of keyword arguments to be passed on to the backup method.
    """
    parser = argparse.ArgumentParser(
        description="Back Up PostgreSQL Database",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog="""
    Examples:
        # Copy existing database to a new database
        python3 backup.py --pgdump --sourcedb BHS_Production --destdb BHS_Testing

        # Copy existing database and save the backup to a file
        python3 backup.py --pgdump --sourcedb BHS_Production --file=/tmp/backups/BHS_Production_20190501.pgdump

        # Copy existing database to a new database and save the backup to a file
        python3 backup.py --pgdump --sourcedb BHS_Production --destdb BHS_Testing --file=/tmp/backups/BHS_Production_20190501.pgdump
        """,
    )

    pg_command_group = parser.add_mutually_exclusive_group(required=True)
    pg_command_group.add_argument("--psql", action="store_true", help="Backup database using a standard psql command")
    pg_command_group.add_argument("--pgdump", action="store_true", help="Backup database using a compressed pg_dump")

    parser.add_argument("--sourcedb", "-s", required=True, help="Name of Source Database (to be backed up)")
    parser.add_argument("--destdb", "-d", help="Name of Destination Database (to be created)")
    parser.add_argument("--file", "-f", help="File name (with full or relative path) where backup should be stored")
    parser.add_argument("--sudo", "-S", action="store_true", help="Run backup/restore commands with sudo")
    parser.add_argument("--user", "-U", help="PostgreSQL user to use for backup and/or restore")
    parser.add_argument("--verbose", "-v", action="store_true")

    arguments = parser.parse_args()

    dest_db = arguments.destdb
    dest_file = arguments.file
    if not (dest_db or dest_file):
        raise parser.error("You must provide a --dest-db (for backing up to a new database) or --dest-file (for backing up to a file)")

    return {
        "backup_type": (arguments.psql and "psql") or (arguments.pgdump and "pg_dump"),
        "source_db": arguments.sourcedb,
        "dest_db": dest_db,
        "dest_file": dest_file,
        "sudo": arguments.sudo,
        "user": arguments.user,
        "verbose": arguments.verbose,
    }


if __name__ == "__main__":
    manage_backup(**parse_arguments())
