# These tests can be run with:
#     python3 -m unittest test_*.py
import unittest
from backup import get_backup_command_pg_dump


class PgDumpBackupTest(unittest.TestCase):
    def test_pg_dump_backup_command(self):
        self.assertEqual(get_backup_command_pg_dump(), "pg_dump -d  -F c")

    def test_pg_dump_backup_command_sourcedb(self):
        self.assertEqual(get_backup_command_pg_dump(source_db="original"), "pg_dump -d original -F c")

    def test_pg_dump_backup_command_destfile(self):
        self.assertEqual(get_backup_command_pg_dump(dest_file="/tmp/backup.pgdump"), "pg_dump -d  -F c -f /tmp/backup.pgdump")

    def test_pg_dump_backup_command_user(self):
        self.assertEqual(get_backup_command_pg_dump(user="odoo"), "pg_dump -d  -F c -U odoo")

    def test_pg_dump_backup_command_verbose(self):
        self.assertEqual(get_backup_command_pg_dump(verbose=True), "pg_dump -d  -F c -v")

    def test_pg_dump_backup_command_sourcedb_destfile(self):
        self.assertEqual(get_backup_command_pg_dump(source_db="original", dest_file="/tmp/backup.pgdump"), "pg_dump -d original -F c -f /tmp/backup.pgdump")

    def test_pg_dump_backup_command_sourcedb_user(self):
        self.assertEqual(get_backup_command_pg_dump(source_db="original", user="odoo"), "pg_dump -d original -F c -U odoo")

    def test_pg_dump_backup_command_sourcedb_verbose(self):
        self.assertEqual(get_backup_command_pg_dump(source_db="original", verbose=True), "pg_dump -d original -F c -v")

    def test_pg_dump_backup_command_destfile_user(self):
        self.assertEqual(get_backup_command_pg_dump(dest_file="/tmp/backup.pgdump", user="odoo"), "pg_dump -d  -F c -U odoo -f /tmp/backup.pgdump")

    def test_pg_dump_backup_command_destfile_verbose(self):
        self.assertEqual(get_backup_command_pg_dump(dest_file="/tmp/backup.pgdump", verbose=True), "pg_dump -d  -F c -f /tmp/backup.pgdump -v")

    def test_pg_dump_backup_command_user_verbose(self):
        self.assertEqual(get_backup_command_pg_dump(user="odoo", verbose=True), "pg_dump -d  -F c -U odoo -v")

    def test_pg_dump_backup_command_sourcedb_destfile_user(self):
        self.assertEqual(
            get_backup_command_pg_dump(source_db="original", dest_file="/tmp/backup.pgdump", user="odoo"),
            "pg_dump -d original -F c -U odoo -f /tmp/backup.pgdump",
        )

    def test_pg_dump_backup_command_sourcedb_destfile_verbose(self):
        self.assertEqual(
            get_backup_command_pg_dump(source_db="original", dest_file="/tmp/backup.pgdump", verbose=True), "pg_dump -d original -F c -f /tmp/backup.pgdump -v"
        )

    def test_pg_dump_backup_command_sourcedb_user_verbose(self):
        self.assertEqual(get_backup_command_pg_dump(source_db="original", user="odoo", verbose=True), "pg_dump -d original -F c -U odoo -v")

    def test_pg_dump_backup_command_destfile_user_verbose(self):
        self.assertEqual(
            get_backup_command_pg_dump(dest_file="/tmp/backup.pgdump", user="odoo", verbose=True), "pg_dump -d  -F c -U odoo -f /tmp/backup.pgdump -v"
        )

    def test_pg_dump_backup_command_sourcedb_destfile_user_verbose(self):
        self.assertEqual(
            get_backup_command_pg_dump(source_db="original", dest_file="/tmp/backup.pgdump", user="odoo", verbose=True),
            "pg_dump -d original -F c -U odoo -f /tmp/backup.pgdump -v",
        )


if __name__ == "__main__":
    unittest.main()
